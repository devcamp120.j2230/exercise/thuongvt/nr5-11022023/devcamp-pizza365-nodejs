$(document).ready(function(){
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

                // khai báo một biến toàn cục để lưu dữ liệu danh sách người dùng
                var gDataOder ="";
                // khai báo một biến toàn cục để lưu dữ liệu danh sách đồ uống
                var gDataDrink ="";
                // khai báo một biến toàn cục lưu dữ liệu thu thập để tạo một đơn mới.
                var gNewObject = {
                    kichCo: "",
                    duongKinh: "",
                    suon: "",
                    salad: "",
                    loaiPizza: "",
                    idVourcher: "",
                    idLoaiNuocUong: "",
                    soLuongNuoc: "",
                    hoTen: "",
                    thanhTien: "",
                    email: "",
                    soDienThoai: "",
                    diaChi: "",
                    loiNhan: ""
                }    
                // khai báo một biến toàn cục lưu dữ liệu thu thập id người dùng.
                var gId = "";
                // khai báo một biến toàn cục lưu dữ liệu thu thập Oderid người dùng.
                var gOderid="";
                 // khai báo một biến toàn cục lưu dữ liệu thu thập trạng thái người dùng.
                 var gStatus={
                    trangThai:""
                 };

                const gVOUCHER_COLS = [ "orderId", "kichCo","loaiPizza","idLoaiNuocUong","thanhTien","hoTen", "soDienThoai","trangThai","chiTiet"];

                const gODER_ID = 0;
                const gKICH_CO = 1;
                const gLOAI_PIZZA = 2;
                const gNUOC_UONG = 3;
                const gTHANH_TIEN = 4 ;
                const gHO_TEN = 5;
                const gSO_DIEN_THOAI = 6;
                const gTRANG_THAI = 7;
                const gCHI_TIET = 8;

            // định nghĩa table  - chưa có data
            var gUserTable = $("#table-order").DataTable( {
            // Khai báo các cột của datatable
            "columns" : [
                { "data" : gVOUCHER_COLS[gODER_ID]  },
                { "data" : gVOUCHER_COLS[gKICH_CO]  },
                { "data" : gVOUCHER_COLS[gLOAI_PIZZA]  },
                { "data" : gVOUCHER_COLS[gNUOC_UONG]  },
                { "data" : gVOUCHER_COLS[gTHANH_TIEN]  },
                { "data" : gVOUCHER_COLS[gHO_TEN]  },
                { "data" : gVOUCHER_COLS[gSO_DIEN_THOAI] },
                { "data" : gVOUCHER_COLS[gTRANG_THAI] },
                { "data" : gVOUCHER_COLS[gCHI_TIET] },
            ],
            // Ghi đè nội dung của cột action, chuyển thành button chi tiết
            "columnDefs": [ 
            {
                "targets": gCHI_TIET,
                "defaultContent": "<i title='Edit' class='fas fa-edit btn-edit' style='color:green; cursor:pointer;'></i> <i title='Delete' class='fas fa-trash-alt btn-delete' style='color:red; cursor:pointer;'></i>"
            }
                ]
            });
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
        // sự kiện load page
        onPageLoading()
        // Gán sự kiện cho nút lọc
        $(document).on("click","#id-button-loc",function(){
            onBtnLocClick();
        });
        // Gán sự kiện cho nút thêm mới đơn hàng
        $(document).on("click",".btn-add",function(){
            onBtnAddNewClick();
        });
        //Gán sự kiện cho nút tạo đơn
        $(document).on("click",".info-user-Confirm",function(){
            onBtnAddOjectClick();
        });
        //Gán sự kiện cho nút sửa
        $(document).on("click",".btn-edit",function(){
            onBtnEditClick(this);
        });
        //Gán sự kiện cho nút cập nhật
        $(document).on("click",".info-user-CapNhat",function(){
            onBtnConfirmClick(this);
        });
        //Gán sự kiện cho nút xoá
        $(document).on("click",".btn-delete",function(){
            onBtnDeleteClick(this);
        });
        //Gán sự kiện cho nút xác nhận modal xoá 
        $(document).on("click","#btn-confirm-delete-user",function(){
            onBtnConfirmDeleteClick(this);
        });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        // Hàm xử lý  load page
        function onPageLoading(){
            "use strict";
            // Lấy Dữ liệu người dùng từ server
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                type: "GET",
                dataType: 'json',
                success: function(responseObject){
                console.log(responseObject);
                gDataOder = responseObject;
                loadDataToTable(gDataOder);
                },
                error: function(error){
                console.assert(error.responseText);
                }
            });
            
            // Lấy Danh sách đồ uống từ server
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
                type: "GET",
                dataType: 'json',
                success: function(responseDrink){
                gDataDrink = responseDrink;
                console.log(gDataDrink);
                },
                error: function(error){
                console.assert(error.responseText);
                }
            });
        };

            // Hàm xưe lý filter Data
        function onBtnLocClick(){
            "use strict";
            var vStatus = $("#Id-filter-Status").val();
            var vPizzaType = $("#Id-filter-Pizza").val();
            var vDataFilter = gDataOder.filter(function (paramOrder, index) {
                    return(vStatus === "all" || (paramOrder.trangThai != null && vStatus.toUpperCase() === paramOrder.trangThai.toUpperCase()))
                    && (vPizzaType === "all" || (paramOrder.loaiPizza != null && vPizzaType.toUpperCase() === paramOrder.loaiPizza.toUpperCase()))
            });
                //gọi hàm load dữ liệu lên bảng
                console.log(vDataFilter);
                loadDataFilterToTable(vDataFilter);
        };

            // Hàm xử lý  hiển thị modal thêm mới đơn hàng
        function onBtnAddNewClick(){
            "use strict";
            $("#modal-add").modal('show');
            // Load danh sách nước uống lên modal thêm mới đơn hàng
            LoadDataDrinkOnModal(gDataDrink);
            // Hàm thay đổi dữ liệu của combo được chọn
            var vSelectChoose = $("#Id-combo");
            vSelectChoose.change(function(){
                changComboPiza(this)
            });
        };

            // Hàm xử lý nút tạo đơn mới
        function onBtnAddOjectClick(){
            //B1 Thu thập dữ liệu từ form 
            getDataNewOject(gNewObject);
            // B2 Kiểm tra thông tin
            var vCheckData = validateDataOject(gNewObject);
            console.log(vCheckData);
            //console.log(gNewObject);
            if(vCheckData){
                //B3 gửi thông tin dữ liệu lên server
                adDataNewOjectInServer(gNewObject)
            }
        };
            // Hàm xử lý nút sửa
        function onBtnEditClick(paramButton){
            var vTableRow = $(paramButton).closest("tr");
            var vRowData = gUserTable.row(vTableRow).data();
            gId = vRowData.id;
            gOderid = vRowData.orderId
            // Gọi Ajax lấy thông tin từ server
            getAjaxDetailOrder(gOderid)
            $("#update-modal").modal('show');
        };

            // Hàm xưe lý khi ấn nút xoá một oder
        function onBtnDeleteClick(paramButton){
            var vTableRow = $(paramButton).closest("tr");
            var vRowData = gUserTable.row(vTableRow).data();
            gId = vRowData.id;
            console.log(gId);
            $("#delete-user-modal").modal("show");
        };

            // Hàm xử lý sự kiện khi ấn nút xoá trên modal xoá
        function onBtnConfirmDeleteClick(){
            getAjaxDeleteOder();

        }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // load data to table
        function loadDataToTable(paramResponseObject) {
            "use strict";
            //Xóa toàn bộ dữ liệu đang có của bảng
            gUserTable.clear();
            //Cập nhật data cho bảng 
            gUserTable.rows.add(paramResponseObject);
            //Cập nhật lại giao diện hiển thị bảng
            gUserTable.draw();
        };

        // load data filter to table
        function loadDataFilterToTable(paramData) {
            "use strict";
            //Xóa toàn bộ dữ liệu đang có của bảng
            gUserTable.clear();
            //Cập nhật data cho bảng 
            gUserTable.rows.add(paramData);
            //Cập nhật lại giao diện hiển thị bảng
            gUserTable.draw();
        };

        // Load Data nước uống lên modal thêm mới đơn hàng;
        function LoadDataDrinkOnModal(paramData){
            var vSelectDrink = $("#Id-select-drink");
            for (var bIndex = 0; bIndex < paramData.length; bIndex++) {
                $("<option/>", {
                    "value": paramData[bIndex].maNuocUong,
                    "text": paramData[bIndex].tenNuocUong,
                }).appendTo(vSelectDrink)
            }
        };

        // Hàm thay dổi giá trị của conbo Pizza
        function changComboPiza(){
            //console.log("hàm đã chạy");
            var vValue = $("#Id-combo").val();
            if(vValue ===""){
                $("#inp-duong-kinh").val("")
                $("#inp-suon-nuong").val("")
                $("#inp-salad").val("")
                $("#inp-so-luong-nuoc").val("")
                $("#inp-gia").val("")
            };
            if(vValue ==="S"){
                $("#inp-duong-kinh").val(20)
                $("#inp-suon-nuong").val(2)
                $("#inp-salad").val(200)
                $("#inp-so-luong-nuoc").val(2)
                $("#inp-gia").val(150000)
            };
            if(vValue ==="M"){
                $("#inp-duong-kinh").val(25)
                $("#inp-suon-nuong").val(4)
                $("#inp-salad").val(300)
                $("#inp-so-luong-nuoc").val(3)
                $("#inp-gia").val(200000)
            };
            if(vValue ==="L"){
                $("#inp-duong-kinh").val(30)
                $("#inp-suon-nuong").val(8)
                $("#inp-salad").val(500)
                $("#inp-so-luong-nuoc").val(4)
                $("#inp-gia").val(250000)
            };
        };
        
        // Hàm thu thập dữ liệu
        function getDataNewOject(paramData){
            "use strict";
            paramData.kichCo = $("#Id-combo").val();
            paramData.duongKinh = $("#inp-duong-kinh").val();
            paramData.suon = $("#inp-suon-nuong").val();
            paramData.salad = $("#inp-salad").val();
            paramData.loaiPizza = $("#Id-Pizza").val();
            paramData.idVourcher = $("#inp-voucher-id").val();
            paramData.idLoaiNuocUong = $("#Id-select-drink").val();
            paramData.soLuongNuoc = $("#inp-so-luong-nuoc").val();
            paramData.hoTen = $("#inp-ho-ten").val();
            paramData.thanhTien = $("#inp-gia").val();
            paramData.email = $("#inp-email").val();
            paramData.soDienThoai = $("#inp-so-dien-thoai").val();
            paramData.diaChi = $("#inp-dia-chi").val();
            paramData.loiNhan = $("#inp-Loi-nhan").val();
        };

        // Hàm Kiểm tra thông tin
        function validateDataOject(paramData){
            "use strict";
            if(paramData.kichCo===""){
                console.log("Bạn chưa nhập combo Pizaa");
                return false;
            };
            if(paramData.loaiPizza=="all"){
                console.log("Bạn chưa chọn loại Pizaa");
                return false;
            };
            if(paramData.idLoaiNuocUong=="all"){
                console.log("Bạn chưa chọn loại đồ uống");
                return false;
            };
            if (paramData.hoTen === "") {
                alert(" Bạn chưa nhập Họ và Tên");
                return false;
              };
            if (paramData.diaChi === "") {
                alert(" Bạn chưa nhập địa chỉ");
                return false;
              };
            var vEmailHopLe = kiemTraEmail(paramData.email);
            if (vEmailHopLe) {
                return true;
             };
             var vSoDienThoaiHople = kiemTraSoDienThoai(paramData.dienThoai)
             if (vSoDienThoaiHople){
               return true;
            }
            return true;
        };

        // Hàm kiểm tra số điện thoại
        function kiemTraSoDienThoai(paramSoDienThoai){
            "use strict";
            if (paramSoDienThoai==""){
                alert("Bạn chưa nhập số điện thoại");
                return false;
            }
            if(!Number(paramSoDienThoai)) {
                alert("Số nhập vào phải là số");
                return false;
            }
                return true;
        };

        // hàm kiểm tra Email
        function kiemTraEmail (paramEmail) {
                "use strict";
            if (paramEmail.trim()==""){
                alert("Bạn chưa nhập Email");
            return false;
            }
                if(paramEmail.trim().includes("@")==false) {
                alert ("Email nhập vào chưa đúng")
                return false;
                }
                else {
                var vPostAcCong = paramEmail.trim().indexOf("@");
                if(vPostAcCong== 0 || vPostAcCong==paramEmail.trim().length - 1) {
                alert("Email nhập vào chưa đúng");
                return false;
                }
            }
            return true;
        };

        //Hàm gửi dữ liệu lên server
        function adDataNewOjectInServer(paramData){
            "use strict";
            // add thông tin người dùng mới lên server
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                type: "POST",
                dataType: 'json',
                contentType:"application/json",
                data:JSON.stringify(paramData),
                success: function(responseObject){
                console.log(responseObject);
                $("#modal-add").modal('hide');
                location.reload();
                },
                error: function(error){
                console.assert(error.responseText);
                }
            });
        };

        // Hàm lấy dữ liệu từ server thông qua gOrderId
        function getAjaxDetailOrder (paraData){
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + paraData,
                type: "GET",
                dataType: 'json',
                success: function(responseObject){
                console.log(responseObject);
                loadDataToModal(responseObject)
                },
                error: function(error){
                console.assert(error.responseText);
                }
            });
        };

        // Load Data to modal Order Detai
        function loadDataToModal (paraData){
            $("#inp-idmd").val(paraData.id);
            $("#inp-oder-idmd").val(paraData.orderId);
            $("#inp-Combomd").val(paraData.kichCo);
            $("#inp-duong-kinhmd").val(paraData.duongKinh);

            $("#inp-suon-nuongmd").val(paraData.suon);
            $("#inp-do-uongmd").val(paraData.idLoaiNuocUong);
            $("#inp-so-luong-nuocmd").val(paraData.soLuongNuoc);
            $("#inp-voucher-idmd").val(paraData.idVourcher);

            $("#inp-Loai-pizzamd").val(paraData.loaiPizza);
            $("#inp-saladmd").val(paraData.salad);
            $("#inp-thanh-tienmd").val(paraData.thanhTien);
            $("#inp-giam-giamd").val(paraData.giamGia);

            $("#inp-ho-tenmd").val(paraData.hoTen);
            $("#inp-emailmd").val(paraData.email);
            $("#inp-so-dien-thoaimd").val(paraData.soDienThoai);
            $("#inp-dia-chimd").val(paraData.diaChi);

            $("#inp-Loi-nhanmd").val(paraData.loiNhan);
            $("#id-select-trang-thai").val(paraData.trangThai);
            $("#inp-ngay-tao-donmd").val(paraData.ngayTao);
            $("#inp-ngay-cap-nhatmd").val(paraData.ngayCapNhat);
        };

        // Hàm xử lý nút cập nhật
        function onBtnConfirmClick(){
            //B1 thu thập thông tin trạng thái
            gStatus.trangThai = $("#id-select-trang-thai").val();
            //console.log(gStatus);
            //B2 Kiểm tra bỏ qua
            //B3 cập nhật trạng thái người dùng thông qua id
            updateStatus();
        };

        // Hàm cập nhật trạng thái một oder mới dữa vào id
        function updateStatus(){
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
                type: "PUT",
                dataType: 'json',
                contentType:"application/json",
                data:JSON.stringify(gStatus),
                success: function(responseObject){
                console.log(responseObject);
                $("#update-modal").modal('hide');
                location.reload();
                },
                error: function(error){
                console.assert(error.responseText);
                }
            });
        };

        //Hàm xoá thông tin Order dựa vào id
        function getAjaxDeleteOder(){
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
                type: "DELETE",
                dataType: 'json',
                contentType:"application/json",
                success: function(responseObject){
                 $("#delete-user-modal").modal('hide');
                location.reload();
                },
                error: function(error){
                console.assert(error.responseText);
                }
            });
        }


        


})