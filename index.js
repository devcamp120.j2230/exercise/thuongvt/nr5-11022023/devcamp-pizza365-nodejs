const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
// Khai báo router
const { DrinkRouter } = require("./app/router/DrinkRouter");
const { OrderRouter } = require("./app/router/OrderRouter");
const { UserRouter } = require("./app/router/UserRouter");
const { VoucherRouter } = require("./app/router/voucherRouter");

// Khai báo model
// const drinkModel = require("./app/model/DrinkModel");
// const orderModel = require("./app/model/OrderModel");
// const userModel = require("./app/model/UserModel");
// const voucherModel = require("./app/model/VoucherModel");


// Khởi tạo Express App
const app = express();

const port = 8000;

// import thư viện mogoose
const mongoose = require("mongoose");
const { orderRouter } = require("./app/router/oder.router");

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

mongoose.set('strictQuery', true);

//middleware static
app.use(express.static(__dirname + '/views'));

// nơi khai báo API
app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname + "/views/pizza365.html"));
})
app.get("/CRUD",(req,res)=>{
    res.sendFile(path.join(__dirname + "/views/CRUD.html"));
})
// kết nối với mongodb
mongoose.connect(`mongodb://localhost:27017/CRUD_Pizza365`, function(error) {
    if (error) throw error;
    console.log('Kết nối thành công với mongoDB');
   })
   
app.use("/",DrinkRouter);
app.use("/",OrderRouter)
app.use("/",UserRouter);
app.use("/",VoucherRouter);
app.use(orderRouter)

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
