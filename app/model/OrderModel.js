
// khai báo thư viện mongoose
const mogoose = require("mongoose");

// khai báo class schema của mongo
const schema = mogoose.Schema;

//khai báo  review schema 
const orderSchema = new schema({
    // trường id
    // _id: {
    //     type: mogoose.Types.ObjectId,
    //     unique: true
    // },
    // trường orderCode
    orderCode:{
        type: String,
        unique: true,
    },
    // trường pizzaSize
    duongKinhCM:{
        type: String,
        required: false
    },
    // trường pizzaType
    loaiPizza:{
        type: String,
        required: true
    },
    // trường voucher
    voucher: {
        type: mogoose.Types.ObjectId,
        ref:"Voucher"
    },
     // trường drink
     loainuocuong: {
        type: mogoose.Types.ObjectId,
        ref:"Drink",
        require: true
    },
    // trường status
    loiNhan:{
        type: String,
        required: true
    },
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now()
    },

    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now()
    }
})

module.exports = mogoose.model("Order",orderSchema);
