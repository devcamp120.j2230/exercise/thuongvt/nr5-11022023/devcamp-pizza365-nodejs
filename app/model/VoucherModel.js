// khai báo thư viện mongoose
const mongoose = require("mongoose");

// khai báo class schema của mongo
const schema = mongoose.Schema;

// khai báo  review schema 
const voucherSchema = new schema({
    // trường id
    // _id: {
    //     type: mongoose.Types.ObjectId
    // }, 
    // _id : {
    //     type:String
    // },
    // trường mã giảm giá
    maVoucher:{
        type: String,
        unique: true,
        required: true
    },
    // trường phần trăm giảm giá
    phanTramGiamGia:{
        type: Number,
        required: true
    },
    // trường ghi chú
    ghiChu:{
        type: String,
        required: false
    },
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now(),
        required: false
    },
    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now(),
        required: false
    }
})

module.exports = mongoose.model("Voucher", voucherSchema);
