// import thư viện mongose 
const mogoose = require("mongoose");

//import các model
const userModel = require("../model/UserModel");
const orderModel = require("../model/OrderModel");
const voucherModel = require("../model/VoucherModel");
const drinkModel = require("../model/DrinkModel");


//tạo odercode
const orderCode = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    let body = req.body;
    console.log(body)
   
    let orderCodeRandom = Math.random().toString().substring(2,5);
    body.orderCode = orderCodeRandom;

    // B2: Validate dữ liệu từ request body
    if (!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email chưa chính xác"
        })
    }
    // Sử dụng userModel tìm kiếm bằng email
    userModel.findOne({email: body.email},(err,user)=>{
        console.log(user)
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            if(!user){ // nếu dữ liệu là null hoặc undefine thì tạo mới một user mới
                userModel.create({
                    _id: mogoose.Types.ObjectId(),
                    hoVaTen: body.hoVaTen,
                    email: body.email,
                    diaChi: body.diaChi,
                    soDienThoai: body.soDienThoai,
                    loiNhan: body.loiNhan
                },(errUser,dataUsser)=>{
                    if(errUser){
                        return res.status(500).json({
                            message: errUser.message,
                            status:"Lỗi khi tạo mới User he"
                        })
                    }
                    else{
                        // tạo một oder mới từ user vừa tạo
                        orderModel.create({
                            _id: mogoose.Types.ObjectId(),
                            orderCode: orderCodeRandom,
                            duongKinhCM: body.duongKinhCM,
                            loaiPizza: body.loaiPizza,
                            voucher: body.voucher.maVoucher,
                            loainuocuong: body.loainuocuong.maNuocUong,
                            loiNhan: body.loiNhan
                        },(errCreatOrder, dataOrder)=>{
                            if(errCreatOrder){
                                return res.status(500).json({
                                    message:errCreatOrder.message,
                                    status: "không tạo mới được order nhé hfghfh"
                                })
                            }
                            else{
                                userModel.findByIdAndUpdate(dataUsser._id,{$push:{orders:dataOrder._id}}).populate("orders").exec((err,data)=>{
                                        if(err){
                                            return res.status(500).json({
                                                message:"không thể tạo dữ liệu"
                                            })
                                        }
                                        else{
                                            return res.status(200).json({
                                                message:"tạo được dữ liệu order mới rồi nhé",
                                                data: dataOrder
                                            })
                                        }
                                    }
                                )
                            }
                        })
                    }
                })
            }
           else{ // nếu user tồn tại thì lấy id của user tạo order
            orderModel.create({
                _id: mogoose.Types.ObjectId(),
                orderCode: orderCodeRandom,
                duongKinhCM: body.duongKinhCM,
                loaiPizza: body.loaiPizza,
                voucher: body.voucher.maVoucher,
                loainuocuong: body.loainuocuong.maNuocUong,
                loiNhan: body.loiNhan
                },(errorCreatedOrder, createdOrder)=>{
                if(errorCreatedOrder){
                    return res.status(500).json({
                        message: errorCreatedOrder.message,
                        status:"không tạo mới được order nhé huhu"
                    })
                }
                else{// phải viết thêm hàm cho loại nước uống và mã voucher
                    // nếu trường hợp có chọn nước uống thì...
                    // nếu trường hợp có mã voucher thì...
                    userModel.findByIdAndUpdate(user._id,{$push:{orders:createdOrder._id}}).populate("orders").exec((err,data)=>{
                            if(err){
                                return res.status(500).json({
                                    message:"không thể tạo dữ liệu"
                                })
                            }
                            else{
                                return res.status(200).json({
                                    message:"tạo được dữ liệu order mới rồi nhé hehe haha",
                                    data: createdOrder
                                })
                            }
                        }
                    )
                }
            })
           } 
        }
    })

}

const getAllDrink = (req,res)=>{
    drinkModel.find().exec((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "all data get successfuly",
                drink: data
            })
        }
    })
}

module.exports = {
    orderCode,
    getAllDrink
}

