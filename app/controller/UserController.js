// import thư viện mongose 
const mongoose = require("mongoose");
// improt UserModel 
const userModel = require("../model/UserModel");

//Tạo function creatUser
const creatUser = (req, res) => {
  //B1 thu thập dữ liệu từ body req
  let body = req.body
  //B2 Kiểm tra dữ liệu
  if (!body.hoVaTen) {
    return res.status(400).json({
      message: 'fullName is required'
    })
  }
  if (!body.email) {
    return res.status(400).json({
      message: 'email is required'
    })
  }
  if (!body.diaChi) {
    return res.status(400).json({
      message: 'address is required'
    })
  }
  if (!body.soDienThoai) {
    return res.status(400).json({
      message: 'phone is required'
    })
  }
  //B3 gọii model thực hiện bài toán tạo mới một User
  let newUser = {
    _id: mongoose.Types.ObjectId(),
    hoVaTen: body.hoVaTen,
    email: body.email,
    diaChi: body.diaChi,
    soDienThoai: body.soDienThoai,
    loiNhan: body.loiNhan
  }
  userModel.create(newUser, (err, data) => {
    if (err) {
      return res.status(500).json({
        message: err.message
      })
    }
    return res.status(201).json({
      message: "Create successfully",
      newUser: data
    })
  })
}
// Tạo function getAllUser
const getAllUser = (req, res) => {
  //B1 Chuẩn bị dữ liệu
  //B2 Kiểm tra dữ liệu
  //B3 gọi model thực hiện nghiệp vụ
  userModel.find((err, data) => {
    if (err) {
      return res.status(500).json({
        message: err.message
      })
    }
    return res.status(201).json({
      message: "get all user successfully",
      Data: data
    })
  })
}

// Tạo API router get all user limit
const getUserLimit = (req, res) => {
  //B1 Chuẩn bị dữ liệu
  let limit = req.query.limit
  //B2 Kiểm tra dữ liệu
  if (!limit || limit == undefined) {
    //B3 gọi model thực hiện nghiệp vụ
    userModel.find()
      .exec((err, data) => {
        if (err) {
          return res.status(500).json({
            message: err.message
          })
        }
        return res.status(201).json({
          message: "get user limit successfully",
          Data: data
        })
      })
  }
  else {
    //B3 gọi model thực hiện nghiệp vụ
    userModel.find()
      .limit(limit)
      .exec((err, data) => {
        if (err) {
          return res.status(500).json({
            message: err.message
          })
        }
        return res.status(201).json({
          message: "get user limit successfully",
          Data: data
        })
      })
  }
}

// Tạo API router get all user skip
const getUserSkip = (req, res) => {
  //B1 Chuẩn bị dữ liệu
  let skip = req.query.skip
  //B2 Kiểm tra dữ liệu nếu skip không có hoặc undefined thì trả ra toàn bộ giá trị
  if (!skip || skip == undefined) {
    userModel.find()
      .exec((err, data) => {
        if (err) {
          return res.status(500).json({
            message: err.message
          })
        }
        return res.status(201).json({
          message: "get user skip successfully",
          Data: data
        })
      })
  }
  else {
    //B3 gọi model thực hiện nghiệp vụ
    userModel.find()
      .skip(skip)
      .exec((err, data) => {
        if (err) {
          return res.status(500).json({
            message: err.message
          })
        }
        return res.status(201).json({
          message: "get user skip successfully",
          Data: data
        })
      })
  }
}

// Tạo API router get all user sorted
const getAllUserSorted = (req, res) => {
  userModel.find()
    .sort({ hoVaTen: "desc" })// tìm kiếm theo thứ tự của biến fullName
    .exec((err, data) => {
      if (err) {
        return res.status(500).json({
          message: err.message
        })
      }
      return res.status(201).json({
        message: "get user sorte successfully",
        Data: data
      })
    })
}

// Tạo API router get all user skip -limit
const getUserLimitSkip = (req, res) => {
  let skip = req.query.skip;
  let limit = req.query.limit
  userModel.find()
    .limit(limit)
    .skip(skip)
    .exec(
      (err, data) => {
        if (err) {
          return res.status(500).json({
            message: err.message
          })
        }
        return res.status(201).json({
          message: "get user successfully",
          Data: data
        })
      }
    )
}
// Tạo API router get all user sorted 
const getUserSortLimitSkip = (req, res) => {
  let skip = req.query.skip;
  let limit = req.query.limit
  userModel.find()
    .sort({ hoVaTen: "desc" })
    .limit(limit)
    .skip(skip)
    .exec(
      (err, data) => {
        if (err) {
          return res.status(500).json({
            message: err.message
          })
        }
        return res.status(201).json({
          message: "get user successfully",
          Data: data
        })
      }
    )
}
// Tạo function getUserById 
const getUserById = (req, res) => {
  //B1 Chuẩn bị dữ liệu
  let Id = req.params.UserId
  //B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(Id)) {
    return res.status(400).json({
      message: "UserId is invalid!"
    })
  }
  //B3 gọi model thực hiện nghiệp vụ
  userModel.findById(Id, (err, data) => {
    if (err) {
      return res.status(500).json({
        message: err.message
      })
    }
    return res.status(201).json({
      message: "get a user by id successfully",
      Data: data
    })
  })
}



// Tạo function updateUserById
const updateUserById = (req, res) => {
  //B1 Chuẩn bị dữ liệu
  let Id = req.params.UserId
  let body = req.body
  //B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(Id)) {
    return res.status(400).json({
      message: 'UserId is invalid!'
    })
  }
  if (!body.hoVaTen) {
    return res.status(400).json({
      message: 'fullName is required'
    })
  }
  if (!body.email) {
    return res.status(400).json({
      message: 'email is required'
    })
  }
  if (!body.diaChi) {
    return res.status(400).json({
      message: 'address is required'
    })
  }
  if (!body.soDienThoai) {
    return res.status(400).json({
      message: 'phone is required'
    })
  }
  //B3 gọi model thực hiện nghiệp vụ
  let newUserUpdate = {
    hoVaTen: body.hoVaTen,
    email: body.email,
    diaChi: body.diaChi,
    soDienThoai: body.soDienThoai
  }
  userModel.findByIdAndUpdate(Id, newUserUpdate, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: err.message
      })
    } else {
      return res.status(200).json({
        status: "Success: update user success by Id",
        data: data
      })
    }
  })
}
// Tạo function deleteUserById
const deleteUserById = (req, res) => {
  //B1 Chuẩn bị dữ liệu
  let Id = req.params.UserId
  //B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(Id)) {
    return res.status(400).json({
      message: 'UserId is invalid!'
    })
  }
  //B3 gọi model thực hiện nghiệp vụ
  userModel.findByIdAndDelete(Id, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "Error 500: Internal server error",
        message: err.message
      })
    } else {
      return res.status(200).json({
        status: "Success: delete user success by Id",
        Data: data
      })
    }
  })
}
//Export thư viện controller thành một modul
module.exports = {
  creatUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById,
  getUserLimit,
  getUserSkip,
  getAllUserSorted,
  getUserLimitSkip,
  getUserSortLimitSkip
}