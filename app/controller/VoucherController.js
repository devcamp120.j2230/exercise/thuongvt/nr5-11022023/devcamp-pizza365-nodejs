// import thư viện mongose 
const mongoose = require("mongoose");

//improt voucherModel
const voucherModel = require("../model/VoucherModel");

//tạo function createVoucher
const createVoucher = (req,res)=>{
    //B1 Thu thập dữ liệu
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if(!body.maVoucher){
        return res.status(400).json({
            message:"Mã voucher chưa hợp lệ"
        })
    }
    if(!Number.isInteger(body.phanTramGiamGia)|| body.phanTramGiamGia < 0){
        return res.status(400).json({
            message:"phần trăm giảm giá chưa hợp lệ"
        })
    }
    //B3 Gọi model thực hiện thao tác nghiệp vụ
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu,
        ngayTao: body.ngayTao,
        ngayCapNhat: body.ngayCapNhat
    }

    voucherModel.create(newVoucher,(err,data)=>{
        if(err){
            console.log(err)
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"creat voucher successfuly",
                newVoucher: data
            })
        }
    })
};

//tạo function getAllVoucher
const getAllVoucher = (req,res)=>{
    //B1 Thu thập dữ liệu
    //B2 Kiểm tra dữ liệu
    //B3 gọi Model thực thi nhiệm vụ
    voucherModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"successfuly load all data",
                 Courses: data
             })
         }
    })
};
//tạo function getVoucherById 
const getVoucherById = (req,res)=>{
    //B1 Thu thập dữ liệu
    let id = req.params.VoucherId;
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 gọi Model thực thi nhiệm vụ
    voucherModel.findById(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(200).json({
                message:"successfuly load data by ID",
                vouCher: data
            })
        }
    })

};
// Tạo function updateVoucherById
const updateVoucherById =(req,res)=>{
    //B1 thu thập dữ liệu
    let id = req.params.VoucherId;
    let body = req.body;
    //B2 kiểu tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    if(!body.maVoucher){
        return res.status(400).json({
            message:"mã giảm giá không hợp lệ"
        })
    }
    if(!Number.isInteger(body.phanTramGiamGia)|| body.phanTramGiamGia < 0){
        return res.status(400).json({
            message:"phần trăm giảm giá chưa hợp lệ"
        })
    }
    //B3 Gọi model thực hiện các nhiệm vụ
    // Khởi toạ một biến thu thập dữ liệu
    let updateVoucher ={
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }

    voucherModel.findByIdAndUpdate(id,updateVoucher,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"successfuly updated data by ID",
                 updateVoucher: data
             })
         }
    })

    
};
//Tạo function deleteVoucherById
const deleteVoucherById = (req,res)=>{
    //B1 thu thập dữ liệu
    let id = req.params.VoucherId
    //B2 Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    }
    //B3 gọi model thực hiện nhiệm vụ
    voucherModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"successfuly delete data by ID",
                 DeleteData: data
             })
         }
    })

    
};

// Export thư viện controller thành một modul
module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}
