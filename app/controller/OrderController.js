// improt thư viện mongose 
const mongoose = require("mongoose");

// improt oderModel
const orderModel = require("../model/OrderModel");
//improt userModel
const userModel = require("../model/UserModel");

// Tạo function createOrder orderofUser
const createOrder = (req, res) => {
    //B1 Thu thập dữ liệu
    let Id = req.params.UserId;
    let body = req.body
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return response.status(400).json({
            status: "Bad Request",
            Message: "UserId is not valid!"
        })
    }
    if (!body.orderCode) {
        return res.status(400).json({
            message: "oderCode không hợp lệ"
        })
    }
    if (!body.duongKinhCM) {
        return res.status(400).json({
            message: "pizzaSize không hợp lệ"
        })
    }
    if (!body.loaiPizza) {
        return res.status(400).json({
            message: "pizzaType không hợp lệ"
        })
    }
    //B3 gọi Model thực thi nhiệm vụ
    let newOrderData = {
        _id: mongoose.Types.ObjectId(),
        orderCode: body.orderCode,
        duongKinhCM: body.duongKinhCM,
        loaiPizza: body.loaiPizza,
        loiNhan: body.loiNhan
    }
    //gọi model oder tạo một oder mới 
    //Sau đó khi tạo xong oder cần thêm idoder mới vào mảng oders của user
    orderModel.create(newOrderData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error1",
                message: err.message
            })
        }
        userModel.findByIdAndUpdate(Id, {
            $push: { orders: data._id }
        }, (err1, data1) => {
            if (err1) {
                return res.status(500).json({
                    status: "Internal server error2",
                    message: err1.message
                })
            }
            return res.status(201).json({
                status: "Create review successfully",
                Data: data  // Data này là data của oder chứ không phải là data1 của user
            })
        })
    })
}
// Tạo function getAllOrder
const getAllOrder = (req, res) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    orderModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "Get all order successfully",
            Data: data
        })
    })
}
// Tạo function getOrderById
const getOrderById = (req, res) => {
    // B1: Chuẩn bị dữ liệu
    let Id = req.params.OrderId;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return response.status(400).json({
            status: "Bad Request",
            Message: "UserId is not valid!"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    orderModel.findById(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Get a order by Id successfully",
            Data: data
        })
    })
}
// Tạo function updateOrderById 
const updateOrderById = (req, res) => {
    // B1: Chuẩn bị dữ liệu
    let Id = req.params.OrderId
    let body = req.body
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return response.status(400).json({
            status: "Bad Request",
            Message: "UserId is not valid!"
        })
    }
    if (!body.orderCode) {
        return res.status(400).json({
            message: "oderCode không hợp lệ"
        })
    }
    if (!body.duongKinhCM) {
        return res.status(400).json({
            message: "pizzaSize không hợp lệ"
        })
    }
    if (!body.loaiPizza) {
        return res.status(400).json({
            message: "pizzaType không hợp lệ"
        })
    }
    if (!body.loiNhan) {
        return res.status(400).json({
            message: "staus không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    let newOderUpdateData = {
        orderCode: body.orderCode,
        duongKinhCM: body.duongKinhCM,
        loaiPizza: body.loaiPizza,
        loiNhan: body.loiNhan
    }
    orderModel.findByIdAndUpdate(Id, newOderUpdateData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Update order  successfully",
            Data: data
        })
    })
}
// Tạo function deleteOrderById
const deleteOrderById = (req,res)=>{
    // B1: Chuẩn bị dữ liệu
    let Id = req.params.OrderId
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return response.status(400).json({
            status: "Bad Request",
            Message: "UserId is not valid!"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    orderModel.findByIdAndDelete(Id,(err,data)=>{
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            status: "Delete order successfully",
            Data: data
        })
    })
}
// Export thư viện controller thành một modul
module.exports = {
    createOrder,// orderofUser
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}