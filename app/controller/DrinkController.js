// khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo model
const DrinkModel = require("../model/DrinkModel");


// Tạo một Drink mới
const createDrink = (req,res)=>{
    //B1 thu thập dữ liệu
    // Tạo một biến thu thập dữ liệu
    let body = req.body;
    console.log(body);
    // B2 Kiểm tra dữ liệu đầu vào chia nhỏ các trường hợp để kiểm tra.
    if(body.maNuocUong === undefined || body.maNuocUong ===""){
        return res.status(400).json({
            status: "Bad Request",
            message: "maNuocUongo is not valid!"
        })
    }
    if(!body.tenNuocUong){
        return res.status(400).json({
            status: "Bad Request",
            message: "Tên nước uống is not valid!"
        })
    }
    if(body.donGia !== undefined && !(Number.isInteger(body.donGia)&&body.donGia>=0)){
        return res.status(400).json({
            status: "Bad Request",
            message: "donGia is not valid!"
        })
    }
    //B3 gọi model thực hiện bài toán
    // B3.1 chuẩn bị dữ liệu
    let newDrinkData = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }
    // sử dụng hàm để tạo một dữ liệu mới
    DrinkModel.create(newDrinkData,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "creat successfuly",
                newCourse: data
            })
        }
    })
}

// lấy toàn bộ drink
const getAllDrink = (req,res)=>{
    //B1 Thu thập dữ liệu
    //B2 Kiểm tra dữ liệu
    //B3 Thực hiện bài toán
    DrinkModel.find((err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "all data get successfuly",
                dink: data
            })
        }
    })
}

// Lấy drink theo Id
const getADrinkById = (req,res)=>{
    //B1 Thu thập dữ liệu
    let Id = req.params.DrinkId;
    //B2 Kiểm tra dữ liệu 
    // Kiểm tra id
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'DrinkId is invaId'
        })
    }
    console.log(Id);
    // B3 Thực hiện bài toán
    DrinkModel.findById(Id,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "successfuly load data by ID",
                Course: data
            })
        }
    })
}

// Sửa 01 Drink theo Id
const updateDrinkById = (req,res)=>{
    //B1 Thu Tập dữ liệu
    let Id = req.params.DrinkId;
    let body = req.body;
    //B2 Kiem tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'DrinkId is invaId'
        })
    }
    console.log(Id);
    if(body.maNuocUong === undefined || body.maNuocUong ===""){
        return res.status(400).json({
            status: "Bad Request",
            message: "maNuocUongo is not valid!"
        })
    }
    if(!body.tenNuocUong){
        return res.status(400).json({
            status: "Bad Request",
            message: "Tên nước uống is not valid!"
        })
    }
    if(body.donGia !== undefined && !(Number.isInteger(body.donGia)&&body.donGia>=0)){
        return res.status(400).json({
            status: "Bad Request",
            message: "donGia is not valid!"
        })
    }
    console.log(body);
    //B3 Thực hiện bài toán
    let newUpdateDrinkData = {
        maNuocUong: body.maNuocUong,
        donGia: body.donGia,
        tenNuocUong: body.tenNuocUong
    }
    DrinkModel.findByIdAndUpdate(Id,newUpdateDrinkData,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "successfuly updated data by ID",
                Course: data
            })
        }
    })
}

//Xóa một drink by Id
const deleteDrinkById = (req,res)=>{
     //B1 Thu thập dữ liệu
     let Id = req.params.DrinkId;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'DrinkId is invaId'
        })
    }
    console.log(Id);
    //B3 Thực hiện bài toán
    DrinkModel.findByIdAndDelete(Id,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "successfuly delete data by ID",
                Course: data
            })
        }
    })
}
//Export thư viện controller thành một modul
module.exports = {
    createDrink,
    getAllDrink,
    getADrinkById,
    updateDrinkById,
    deleteDrinkById
}